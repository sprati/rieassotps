var app = document.getElementById('app');

document.addEventListener('DOMContentLoaded', main);

async function main() {

    var list = document.createElement('ul');
    list.id = 'list';

    render();
}

async function render() {
    var response = await fetch('/todo');
    var formatJSON = response.json();

    var list = document.createElement('ul');
    list.innerHTML = "";

    for (var i = 0; i < formatJSON.length; i++) {
        var elemetList = document.createElement('li');
        var span = document.createElement('span');

        var todo = formatJSON[i];

        span.textContent = todo.id + " - " + todo.description;

        if (todo.done)
            span.textContent += '- [done]';
        else
            span.textContent += '- [not done]'
.
        elemetList.appendChild(span);
        list.appendChild(elemetList);
    }
}
