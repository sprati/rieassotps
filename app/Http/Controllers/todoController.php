<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class todoController extends Controller
{

    public function add(Request $request) {

        $description = $request->input('description');

        $result = app('db')->insert("0INSERT INTO todo (description, done, date_insert) VALUES ($description, false, now())");

        return new Response(null, 201);
    }

    public function update(Request $request, $id) {

        $description = $request->input('description');
        $done = $request->input('done');

        $result = app('db')->update("UPDATE todo SET description='$description', done=$done WHERE id=$id");

        return new Response(null, 200);
    }

    public function delete(Request $request, $id) {

        $result = app('db')->delete("DELETE FROM todo WHERE id=$id");

        return new Response(null, 204);
    }
}
